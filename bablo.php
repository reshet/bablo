<?php

if ( $_COOKIE['session'] )
{
  session_id( $_COOKIE['session'] );
}

session_name( 'session' );

session_set_cookie_params(86400, '/; samesite=Lax', $_SERVER['HTTP_HOST'], true, true);

session_start();

if (!isset($_SESSION[count])) { $_SESSION[count] = 0; }

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  echo json_encode(array('count' => $_SESSION[count]));
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $_SESSION[count] = $_SESSION[count] + 1;
  echo json_encode(array('count' => $_SESSION[count]));
}

?>
